#!/usr/bin/env bash
echo ""
echo "Warning: This script may take a number of days to run"
echo "reduce the size of the iterations variable below to"
echo "a smaller number (e.g. 10, 5) to run relatively quickly"
echo ""
read -n 1 -s -r -p "       Press any key to continue                                                             "
echo ""
iterations=25
cd code/figure-5-4/
python3 iterate_trainings_stepwise.py $iterations

