#!/usr/bin/env bash
echo ""
echo "This script assumes that you have libreoffice installed:"
echo "the file /code/figure-5-3 should open in most spreadsheet programs"
echo "(e.g. libreoffice, openoffice, Microsoft Excel)"
echo ""
libreoffice ./code/figure-5-3/effect_of_loss_function.ods
