#!/usr/bin/env python3
## run as python3 figure-5-5-correlation-graph2.py /media/andrew/BackupDisk1/200511_smr_sobol/runs/ 3454320

import pandas as pd
import sys, os
import numpy as np
import time
import random as rn
import tensorflow as tf
from data_store import data_store

from timeit import default_timer as timer

#debug ndf_only
import matplotlib.pyplot as plt
import matplotlib.font_manager as font_manager
fig_font = {'fontname':'Liberation Serif'}
font = font_manager.FontProperties(family='Liberation Serif')

seed  = sys.argv[2]
# Max for PYTHONHASHSEED 4294967295
# Max for numpy 2**32
if seed == 'time':
    # shuffled time initialisation source:
    # https://github.com/schmouk/PyRandLib
    #t = int( time.time() * 1000.0 )
    #seed = str( ( ((t & 0x0f000000) >> 24) +
    #            ((t & 0x00ff0000) >>  8) +
    #            ((t & 0x0000ff00) <<  8) +
    #            ((t & 0x000000ff) << 24)   )  )
    seed = str(int( (time.time()*1000.0) % 2147483647))
#os.environ['PYTHONHASHSEED'] = '0'
np.random.seed(int(seed))
#rn.seed(np.random.randint(0, 2**32 - 1, dtype='l'))
rn.seed(np.random.randint(0, 2147483647, dtype='l'))

#
# for 1.15, but not 2.0 where eager execution is set...
#from keras import backend as K
#tf.set_random_seed(np.random.randint(0, 2147483647, dtype='l'))
#session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)
#sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)
#K.set_session(sess)
tf.random.set_seed(int(seed))

from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.models import save_model
from keras.models import load_model
#from keras.models import model_from_json
from keras.optimizers import Adam
from sklearn.preprocessing import MinMaxScaler, RobustScaler
from sklearn.model_selection import train_test_split
import data_store as ds
import pickle

from keras.layers import Conv2D, AveragePooling2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.layers.core import Reshape

from PIL import Image
import copy

class record:
    pass

mlp_layers = 3
neurons_per_layer = 250

description = "\nA script to build a CNN of microcore assemblies\n\n"
example_usage = "usage : \n" + str(sys.argv[0])+ " directory of pickle files time\n\n eg:\n python3 "+str(sys.argv[0])+" ../simple_nn/results_190430_serp/runs time\n\n"

def usage_error(string = ""):
    print ("\n\nUSAGE ERROR: \n"+string)
    print (str(sys.argv[0]) + description + example_usage)
    quit()


def get_hot_pin_ppf(self, d):
    # takes a list of assembly powers and finds
    # the ppf and the position of the hot pin7
    x = np.zeros((17, 17))
    full = np.block([[d[0], d[1], d[2], d[3]],
                     [x   , d[4], d[5], d[6]],
                     [x   , d[7], d[8], d[9]],
                     [x   , d[10],x,    x   ] ])
    #print(np.shape(full))
    hot_pin = np.unravel_index(full.argmax(), full.shape)
    max = np.max(full)
    full[full == 0.0] = np.nan
    mean_no_zero = np.nanmean(full.ravel())
    ppf = max / mean_no_zero
    #print(str(mean_no_zero))
    #print(str(ppf))
    return (list(hot_pin) + [ppf])

def output_bitmask():
    w, h = 68, 68
    ten_vars = [1] * 10
    input_lut       = [ 0, 1, 2, 3,99, 1, 4, 5, 6,99, 2, 7, 8,99,99, 3, 9,99,99,99,99,99,99,99,99]
    control_rod_lut = [ 0, 1, 2, 3,99, 4, 5, 6, 7,99, 8, 9,10,99,99,11,12,99,99,99,99,99,99,99,99]
    control_rods_zero = [[[ 5,  2], [ 8,  2], [12,  2],
                    [ 3,  3], [13,  3],
                    [ 2,  5], [ 5,  5], [ 8,  5], [11,  5], [14,  5],
                    [ 2,  8], [ 5,  8], [ 8,  8], [11,  8], [14,  8],
                    [ 2, 11], [ 5, 11], [ 8, 11], [11, 11], [14, 11],
                    [ 3, 13], [13, 13], [ 5, 14],
                    [ 8, 14], [11, 14]]
                   ]
    control_rods_zero.append([ [v[0]+17,v[1]+ 0] for v in control_rods_zero[0] ])
    control_rods_zero.append([ [v[0]+34,v[1]+ 0] for v in control_rods_zero[0] ])
    control_rods_zero.append([ [v[0]+51,v[1]+ 0] for v in control_rods_zero[0] ])
    control_rods_zero.append([ [v[0]+ 0,v[1]+17] for v in control_rods_zero[0] ])
    control_rods_zero.append([ [v[0]+17,v[1]+17] for v in control_rods_zero[0] ])
    control_rods_zero.append([ [v[0]+34,v[1]+17] for v in control_rods_zero[0] ])
    control_rods_zero.append([ [v[0]+51,v[1]+17] for v in control_rods_zero[0] ])
    control_rods_zero.append([ [v[0]+ 0,v[1]+34] for v in control_rods_zero[0] ])
    control_rods_zero.append([ [v[0]+17,v[1]+34] for v in control_rods_zero[0] ])
    control_rods_zero.append([ [v[0]+34,v[1]+34] for v in control_rods_zero[0] ])
    control_rods_zero.append([ [v[0]+ 0,v[1]+51] for v in control_rods_zero[0] ])
    control_rods_zero.append([ [v[0]+17,v[1]+51] for v in control_rods_zero[0] ])

    data = np.zeros((h, w, 1), dtype=np.uint8)
    for x in range(w):
        for y in range(h):
            assembly_number = input_lut [int(x/17) + int(y/17) * 5]
            control_number  = control_rod_lut[int(x/17) + int(y/17) * 5]
            if x%17 == 0 or y%17==0 or assembly_number > 10:  # water gap or outside
                data[y, x] = 0
            else:
                if [x,y] in control_rods_zero[control_number] or 0== x or 0==y or w-1 == x or h-1 == y:
                    #print(control_number)
                    data[y, x] = 0
                else:
                    data[y, x] = 1
    return data




def get_input_image(ten_vars, filename='input_img.png', save=False):
    w, h = 73, 73
    #input_lut       = [[0,1,2,3,99],[1,4,5,6,99],[2,7,8,99,99],[3,9,99,99,99],[99,99,99,99,99]]
    #control_rod_lut = [[0,1,2,3,99],[4,5,6,7,99],[8,9,10,99,99],[11,12,99,99,99],[99,99,99,99,99]]
    input_lut       = [ 0, 1, 2, 3,99, 1, 4, 5, 6,99, 2, 7, 8,99,99, 3, 9,99,99,99,99,99,99,99,99]
    control_rod_lut = [ 0, 1, 2, 3,99, 4, 5, 6, 7,99, 8, 9,10,99,99,11,12,99,99,99,99,99,99,99,99]
    control_rods = [[[ 6,  3], [ 9,  3], [12,  3],
                    [ 4,  4], [14,  4],
                    [ 3,  6], [ 6,  6], [ 9,  6], [12,  6], [15,  6],
                    [ 3,  9], [ 6,  9], [ 9,  9], [12,  9], [15,  9],
                    [ 3, 12], [ 6, 12], [ 9, 12], [12, 12], [15, 12],
                    [ 4, 14], [14, 14], [ 6, 15],
                    [ 9, 15], [12, 15]]
                   ]
    control_rods.append([ [v[0]+18,v[1]+ 0] for v in control_rods[0] ])
    control_rods.append([ [v[0]+36,v[1]+ 0] for v in control_rods[0] ])
    control_rods.append([ [v[0]+54,v[1]+ 0] for v in control_rods[0] ])
    control_rods.append([ [v[0]+ 0,v[1]+18] for v in control_rods[0] ])
    control_rods.append([ [v[0]+18,v[1]+18] for v in control_rods[0] ])
    control_rods.append([ [v[0]+36,v[1]+18] for v in control_rods[0] ])
    control_rods.append([ [v[0]+54,v[1]+18] for v in control_rods[0] ])
    control_rods.append([ [v[0]+ 0,v[1]+36] for v in control_rods[0] ])
    control_rods.append([ [v[0]+18,v[1]+36] for v in control_rods[0] ])
    control_rods.append([ [v[0]+36,v[1]+36] for v in control_rods[0] ])
    control_rods.append([ [v[0]+ 0,v[1]+54] for v in control_rods[0] ])
    control_rods.append([ [v[0]+18,v[1]+54] for v in control_rods[0] ])

    data = np.zeros((h, w, 3), dtype=np.uint8)
    for x in range(w):
        for y in range(h):
            assembly_number = input_lut [int(x/18) + int(y/18) * 5]
            control_number  = control_rod_lut[int(x/18) + int(y/18) * 5]
            if x%18 == 0 or y%18==0 or assembly_number > 10:  # water gap or outside
                data[y, x] = [0, 0, 255]
            else:
                if [x,y] in control_rods[control_number] or 0== x or 0==y or w-1 == x or h-1 == y:
                    #print(control_number)
                    data[y, x] = [0, 0, 255]
                else:
                    data[y, x] = [ten_vars[assembly_number-1]*50, 0, 0]
                    #print([x,y])
    #print(control_rods)
    data = data[9:,10:]
    if save:
        img = Image.fromarray(data, 'RGB')
        filename = "test.png"
        img.save(filename, "PNG")
    return data

def get_output_hp_ppf(d):
    full = copy.deepcopy(d)
    full[full == 0.0] = np.nan
    max = np.nanmax(full)
    mean_no_zero = np.nanmean(full.ravel())
    ppf = max / mean_no_zero
    hot_pin = np.unravel_index(full.argmax(), full.shape)
    return (list(hot_pin) + [ppf])

def get_output_image(d, filename='pin_power.png', save=False):
    print(d)
    full_smr  =  copy.deepcopy(np.zeros([68,68]))
    full_smr[ 0:17,  0:17] = d[0] * 4
    full_smr[ 0:17, 17:34] = d[1] * 2
    full_smr[ 0:17, 34:51] = d[2] * 2
    full_smr[ 0:17, 51:68] = d[3] * 2
    full_smr[17:34, 17:34] = d[4]
    full_smr[17:34, 34:51] = d[5]
    full_smr[17:34, 51:68] = d[6]
    full_smr[34:51, 17:34] = d[7]
    full_smr[34:51, 34:51] = d[8]
    full_smr[51:68, 17:34] = d[9]
    #cmap = plt.cm.viridis
    #cmap.set_bad((1, 1, 1, 0))
    #plt.imshow(full_smr, cmap='viridis')
    #plt.colorbar()
    #plt.show()
    #exit()

    full = copy.deepcopy(full_smr)
    full[full == 0.0] = np.nan
    max = np.nanmax(full) # largest value
    min = np.nanmin(full)
    full[full == np.nan] = 0.0
    old_range = max - min
    scaled = ( ( (full -min) * 1)/ old_range)+0.2
    scaled[scaled == np.nan] = 0
    #img.convert('RGB')
    if save:
        img = Image.fromarray(scaled*255)
        print(full)
        img.show()
        new_p = img.convert("L")
        new_p.save(filename, "PNG")
    return full

#    w, h = 55, 55
#    control_rods = [[[ 6,  3], [ 9,  3], [12,  3],
#                    [ 4,  4], [14,  4],
#                    [ 3,  6], [ 6,  6], [ 9,  6], [12,  6], [15,  6],
#                    [ 3,  9], [ 6,  9], [12,  9], [15,  9],
#                    [ 3, 12], [ 6, 12], [ 9, 12], [12, 12], [15, 12],
#                    [ 4, 14], [14, 14], [ 6, 15],
#                    [ 9, 15], [12, 15]]
#                   ]
#    control_rods.append([ [v[0]+18,v[1]+ 0] for v in control_rods[0] ])
#    print(control_rods)
#    control_rods.append([ [v[0]+36,v[1]+ 0] for v in control_rods[0] ])
#    control_rods.append([ [v[0]+ 0,v[1]+18] for v in control_rods[0] ])
#    control_rods.append([ [v[0]+18,v[1]+18] for v in control_rods[0] ])
#    control_rods.append([ [v[0]+36,v[1]+18] for v in control_rods[0] ])
#    control_rods.append([ [v[0]+ 0,v[1]+36] for v in control_rods[0] ])
#    control_rods.append([ [v[0]+18,v[1]+36] for v in control_rods[0] ])
#    control_rods.append([ [v[0]+36,v[1]+36] for v in control_rods[0] ])
#
#    data = np.zeros((h, w, 3), dtype=np.uint8)
#    for x in range(w):
#        for y in range(h):
#            assembly_number = int(x/18) + (int(y/18) *3)
#            print(str(assembly_number))
#            if x%18 == 0 or y%18==0:  # water gap
#                data[y, x] = [0, 0, 255]
#            else:
#                if [x,y] in control_rods[assembly_number] or 0== x or 0==y or w-1 == x or h-1 == y:
#                    data[y, x] = [0, 0, 255]
#                else:
#                    data[y, x] = [nine_vars[assembly_number]*50, 0, 0]
#    img = Image.fromarray(data, 'RGB')
#    return img



def main():

    if 3 != len(sys.argv) or sys.argv[1] == "--help":
        usage_error()
    pickle_dir = sys.argv[1]
    seed  = sys.argv[2]
    if seed == 'time':
        seed = str(int( (time.time()*1000.0) % 2147483647))
    # import the specified sheet of the data
    np.random.seed(int(seed))
    #rn.seed(np.random.randint(0, 2**32 - 1, dtype='l'))
    rn.seed(np.random.randint(0, 2147483647, dtype='l'))

    try:
        os.path.exists(pickle_dir)
    except:
        print("\n\ directory not found: " + excel_file + "\n")
        exit()
    #list files (just the pickles)
    file_list = [x for x in os.listdir(pickle_dir) if x.endswith(".pickle")]
    #run through the files in order.
    #print (str(file_list))
    inputs = []
    outputs = []
    for data_file in sorted(file_list, key=lambda a: a.split(".")[0]):
        print(pickle_dir+"/"+data_file)
        try:
            pickle_file = open(pickle_dir+"/"+data_file, "rb")
            print("opened file")
            var = pickle.load(pickle_file, encoding='latin1')
            pickle_file.close()
        except:
            print("error opening '"+data_file+"' pickle file")
            exit()
        else:
            print(var.enrichments)
            inputs.append(get_input_image(var.enrichments)) #, save=True))
            #outputs.append( np.concatenate( var.detector_data, axis=0 ) )
            #outputs.append([get_output_image(var.detector_data)]+ [get_hot_pin_ppf(var.detector_data)])
            outputs.append(get_output_image(var.detector_data))#, save=))
#            np.set_printoptions(threshold=np.inf)
#            print(outputs[0])
#            exit()
#    num_inputs = len(inputs[0])
    num_outputs= 68*68

    #scales...
    #in_scaler = MinMaxScaler()
    #print(in_scaler.fit(inputs))
    #X = in_scaler.transform(data)

    X =  np.multiply(np.array(inputs), 1.0/5 )

#    print(np.shape([x[0] for x in outputs]))
#    print(np.shape([x[1] for x in outputs]))
    pin_pows =  outputs
#    print("nan in pin_pows:" +str(np.isnan(np.sum(pin_pows))))
    max = np.nanmax(np.array(pin_pows).flatten()) # largest value
#    print("the max values is : " + str(max))
    min = np.nanmin(np.array(pin_pows).flatten())
#    print("the min values is : " + str(min))
    pin_pows = np.nan_to_num(pin_pows)
#    print("nan in pin_pows:" +str(np.isnan(np.sum(pin_pows))))
    #outputs[outputs == np.nan] = 0.0
    old_range = max-min
    print(max)
    print(min)
    print(old_range)
#
    #Y = [[np.multiply(np.array(x[0]),scale_factor)]+[x[1]] for x in outputs]
    old_range = max-min
    offset = 0.0
    scale_factor = ( ( (1 -min) * 1) / old_range)+0.1
    print(scale_factor)
    scaled_set = ( ( (pin_pows -min) * 1)/ old_range) + offset
    save_scale_factors = ["list of format: desc, input_range, max, min, offset", old_range, max, min, offset]
    Y = scaled_set
    print(Y)
    print("nan in outputs:" +str(np.isnan(np.sum(outputs))))
    print("nan in Y:" +str(np.isnan(np.sum(Y))))

    #X, Y = preprocess(inputs, outputs)
#    print(X[0].shape)
#    print(Y[0].shape)
    #X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size = 0.2)
    X_train = X[:800]
    X_test  = X[800:]
    Y_train = Y[:800]
    Y_test  = Y[800:]

    print(np.isnan(np.sum(X_train)))
    print(np.isnan(np.sum(X_test)))
    print(np.isnan(np.sum(Y_train)))
    print(np.isnan(np.sum(Y_test)))

# split the ppf and hotpins from the pin power list...
#    Y_train_ppf_hot = Y_traind[:][:][0][-1]
#    Y_test_ppf_hot = Y_testd[:][:][0][-1]
#    Y_train = Y_traind[:][:][:][0]
#    Y_test = Y_testd[:][:][:][0]

#    for v1,v2 in zip(Y_train, Y_train_ppf_hot):
#        full=np.array(v1)
#        hot_pin = np.unravel_index(full.argmax(), full.shape)
#        max = np.max(full)
#        full[full == 0.0] = np.nan
#        mean_no_zero = np.nanmean(full.ravel())
#        ppf = max / mean_no_zero
#        exit()
#        print(v2)
#        print(ppf)
#        error = np.absolute(ppf-v2[2])
#        print(error)
#        exit()
#    exit()


#    print(X_train[0].shape)
#    exit()
    #print(X_test)
    #print(Y_test)
    # Create a neural network: (dimensions will be based on heatmap work)
    model = Sequential()
    model.add(Conv2D(3, (3, 3), input_shape=(64, 63, 3), name="conv_one", kernel_initializer='random_uniform' ))
    model.add(Activation('tanh'))
    model.add(AveragePooling2D(pool_size=(2, 2)))
    #model.add(MaxPooling2D())

    model.add(Conv2D(6, (3, 3), name="conv_two", kernel_initializer='random_uniform'))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    #model.add(MaxPooling2D())

    model.add(Conv2D(3, (3, 3), name="conv_three", kernel_initializer='random_uniform' ))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    #model.add(MaxPooling2D())

    # the model so far outputs 3D feature maps (height, width, features)
    model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
#    model.add(Dense(64))
#    model.add(Activation('relu'))

    model.add(Dense(3000, kernel_initializer='normal', activation='tanh'))
    for n in range(mlp_layers):
        model.add(Dense(neurons_per_layer, kernel_initializer='normal', activation='tanh'))
    model.add(Dense(num_outputs, kernel_initializer='normal'))
    model.add(Reshape((68, 68)))
#    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mae']) #
#    model.compile(loss='mean_squared_logarithmic_error', optimizer='adam', metrics=['mae']) #
    model.compile(loss='mae', optimizer='adam', metrics=['mae'])
    model.summary()
    # train the model
    # train the model
    start_tr = timer()
    model.fit(X_train, Y_train, epochs=100, batch_size=50, verbose=1)
#    model.fit(X_train, Y_train, epochs=10, batch_size=100, verbose=1)
    end_tr = timer()
    # test
    scores = model.evaluate(X_test, Y_test)
    print("\nperformance on test set:" + str(scores[1]))
    print("scores:"+str(scores))

    start_t = timer()
    predictions = model.predict(X_test)
    end_t = timer()
    num_runs = len(X_test)

    print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))

    rms_out = [0] * len(Y_test)
    for y, y_hat in zip(Y_test, predictions):
        for i, (val, val_hat) in enumerate(zip (y, y_hat)):
            rms_out[i] += np.absolute((val-val_hat))
    rms_out2 = [x / len(Y_test) for x in rms_out]
    rms_error = [x for x in rms_out2]

    rms_out = [0] * num_outputs
    overall_errors = []
    pp =[]
    ppf_vals=[]
    pin_var = []
    pred_vals=[]
    err=[]
    for y, y_hat in zip(Y_test, predictions):
        #print(np.shape(y))
        hp_ppf = get_output_hp_ppf(y)
        hp_ppf_hat =get_output_hp_ppf(np.multiply(y_hat, output_bitmask()) )
        e_temp =[]
        why = copy.deepcopy(y)
        why[why == 0.0] = np.nan
        var = np.nanvar(why)
        for val, pred in zip (hp_ppf, hp_ppf_hat):
            e_temp.append(np.absolute(val - pred))
        pp.append(hp_ppf[-1])
        for i, (val, val_hat) in enumerate(zip (y, y_hat)):
            rms_out[i] += np.absolute((val-val_hat))
        overall_errors.append(e_temp)
        print(str(hp_ppf[-1]) + "   "+ str(hp_ppf_hat[-1]) + "   " + str( ( hp_ppf[-1] - hp_ppf_hat[-1]) / hp_ppf[-1]))
        ppf_vals.append(hp_ppf[-1])
        pred_vals.append(hp_ppf_hat[-1])
        pin_var.append(var)
        err.append(( hp_ppf[-1] - hp_ppf_hat[-1]) / hp_ppf[-1])
    rms_out2 = [x / len(Y_test) for x in rms_out]
    rms_error = [x for x in rms_out2]
    np_oe = np.array([w[:1] for w in overall_errors])
    np_pp_abs = np.array([w[-1] for w in overall_errors])
    np_pp = np.array([w[-1]/pp for w,pp in zip(overall_errors, pp)])
    #print(np_pp)
    #print ("results: "+str(overall_errors) )
    print ("averages:  "+str(np_oe.mean()) )
    print ("averages:  "+str(np_pp.mean()) )


    # THERE IS A BUG HERE:
    #print("rms error per variable:"+str(rms_error))
    model_file = "cnn_model_smr.hdf5"
    model.save(model_file)
    model.save_weights("weights_"+model_file)
#    plt.scatter(ppf_vals, pred_vals, alpha=0.45, c =  np.absolute(pred_vals - ( np.multiply((1.95/3.0), ppf_vals) +0.65) ), cmap='cividis') #viridis
#    plt.scatter(ppf_vals, pred_vals, alpha=0.45, c =  np.absolute(pred_vals - ( np.multiply((1.95/3.0), ppf_vals) +0.65) ), cmap='cividis') #viridis
    plt.scatter(ppf_vals, pred_vals, alpha=0.45, c ="#40269d") #viridis
    plt.title('Correlation of actual and predicted PPF for a CNN', **fig_font)
    plt.xlabel('Actual PPF', **fig_font)
    plt.ylabel('Predicted PPF', **fig_font)
    xcoords= [3.0, 6.45]
    ycoords= [2.55,5.1]
    #ax.set_title('Correlation of actual and predicted PPF for a CNN', **fig_font)
    #ax.set_ylabel('Predicted PPF', **fig_font)
    #ax.set_xlabel('Actual PPF', **fig_font)
    # based on :https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
    a = (( ycoords[1]-ycoords[0]) / (xcoords[1]-xcoords[0]))
    b = -1
    c = ycoords[0] - (a*xcoords[0])
    plt.plot(xcoords, ycoords, c='red', alpha=0.6, linestyle='dashed', label= "y = {a:2.2f}x + {c:2.2f}".format(a=a,c=c))
    distances=[]
    for x,y in zip (ppf_vals, pred_vals):
        #d = (np.abs(a*x)+np.abs(b*y)+np.abs(c) )/np.sqrt(a**2 + b**2)
        d = (np.abs(a*x + b*y + c) )/np.sqrt(a**2 + b**2)
        #d = np.linalg.norm([a*x, b*y])/np.sqrt(a**2 + b**2)
        distances.append(d)
        print(d)
    bandwidth =np.percentile(distances, 90)
    print("distance of 90th percentile: ")
    print(bandwidth)
    plt.fill_between(xcoords, ycoords+bandwidth, ycoords-bandwidth, alpha=.25, label= "90% of data points in this interval")
    plt.legend(loc=2, prop=font)
    plt.show()

    plt.scatter(ppf_vals, pin_var, alpha=0.45, c =  np.absolute(pred_vals - ( np.multiply((1.95/3.0), ppf_vals) +0.65) ), cmap='cividis') #viridis
    plt.title('Correlation of actual PPF and pin variance', **fig_font)
    plt.xlabel('Actual PPF', **fig_font)
    plt.ylabel('Actual pin variance', **fig_font)
    #ax.set_title('Correlation of actual and predicted PPF for a CNN', **fig_font)
    #ax.set_ylabel('Predicted PPF', **fig_font)
    #ax.set_xlabel('Actual PPF', **fig_font)
    plt.show()
    print("Please note that this script is temperamental -- if it didn't work try running it a few times - this is to do with the reliability of tensorflow...")


    exit()
    filename = "stats2.pickle"
    if os.path.isfile(filename):
        stats = pickle.load( open( filename, "rb" ) )
        stats.errors1.append(scores[1])
        stats.scores_direct.append(scores)
        stats.per_category_error.append(rms_error)
        stats.np_oe.append(np_oe)
        stats.np_pp1.append(np_pp)
        stats.pp1.append(np.mean(pp))
        stats.np_pp_abs.append(np_pp_abs)
        stats.runtime.append((end_t - start_t) / num_runs)
        stats.traintime.append((end_tr - start_tr))
        pickle.dump(stats, open( filename, "wb" ) )
    else:
        stats = record()
        stats.first = True
        stats.errors1 = [scores[1]]
        stats.scores_direct = [scores]
        stats.per_category_error = [rms_error]
        stats.np_oe = [np_oe]
        stats.np_pp1 = [np_pp]
        stats.np_pp_abs = [np_pp_abs]
        stats.runtime = [((end_t - start_t) / num_runs)]
        stats.traintime = [(end_tr - start_tr)]
        stats.metrics = model.metrics_names
        pickle.dump(stats, open( filename, "wb" ) )
        print (stats.errors1)
        print ("Running average over seeds is: " + str(np.mean(stats.errors1))+"\n\n")

    ds=data_store()
    ds.scale_data = save_scale_factors
    filename = "scale.pickle"
    with open(filename, "wb") as f:
        pickle.dump(ds, f)
    print ("metrics: "+str(model.metrics_names))
    print ("scores:  "+str(scores))
    print("size of test set: "+str(num_runs))
    print("initialised and trained conv2d:")
    print("scores:" + str(scores))


    model_load = Sequential()
    model_file = "cnn_model_smr_v2.hdf5"
    model.save(model_file)
    model.save_weights("weights_"+model_file)
    model_load = load_model(model_file)
    model_load.summary()

    model = Sequential()
    model.add(Conv2D(3, (3, 3), input_shape=(64, 63, 3), name="conv_one", kernel_initializer='random_uniform' ))
    model.layers[0].set_weights(model_load.layers[0].get_weights())
    model.add(Activation('tanh'))
    model.add(AveragePooling2D(pool_size=(2, 2)))
    #model.add(MaxPooling2D())

    model.add(Conv2D(6, (3, 3), name="conv_two", kernel_initializer='random_uniform'))
    model.layers[3].set_weights(model_load.layers[3].get_weights())
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    #model.add(MaxPooling2D())

    model.add(Conv2D(3, (3, 3), name="conv_three", kernel_initializer='random_uniform' ))
    model.layers[6].set_weights(model_load.layers[6].get_weights())
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    #model.add(MaxPooling2D())
    # the model so far outputs 3D feature maps (height, width, features)
    model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
#    model.add(Dense(64))
#    model.add(Activation('relu'))

    model.add(Dense(3000, kernel_initializer='normal', activation='tanh'))
    for n in range(mlp_layers):
        model.add(Dense(neurons_per_layer, kernel_initializer='normal', activation='tanh'))
#    for l in range(11,14):
#        model.layers[l].set_weights(model_load.layers[l].get_weights())
    model.add(Dense(num_outputs, kernel_initializer='normal'))
    model.add(Reshape((68, 68)))
#    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mae']) #
#    model.compile(loss='mean_squared_logarithmic_error', optimizer='adam', metrics=['mae']) #
    model.compile(loss='mae', optimizer='adam', metrics=['mae'])
    model.summary()



#    # test
#    #input_layer = InputLayer(input_shape=(480, 720, 3), name="input_1")
#    #model.layers[0] = input_layer
#    new_input_shape = (64,63,3)
#    model.layers[0].batch_input_shape = new_input_shape
#
#    #model.layers[0].set_weights(model.get_layer(name=layer.name).get_weights())
#    #input_layer = Conv2D(3, (3, 3), input_shape=(64, 63, 3), name="replacement_one", kernel_initializer='random_uniform' )
#    #input_layer.set_weights(model.get_layer(name="conv_one").get_weights())
#    #model.layers[0]= input_layer
#    model.summary()
#
#    #model.layers.pop()
#    #model.layers.pop()
#    hidden = Dense(120, activation='relu')(model.layers[-2].output)
#    hidden = Dense(120, activation='relu')(model.layers[-1].output)
#    model.add(Dense(num_outputs, kernel_initializer='normal'))
#    model.add(Reshape((68, 68)))
#    model.summary()
#    model.compile(loss='mae', optimizer='adam', metrics=['mae'])
#    model.summary()
    model.fit(X_train, Y_train, epochs=100, batch_size=50, verbose=0)
    scores_loaded = model.evaluate(X_test, Y_test)
    predictions = model.predict(X_test)

    print("Reloaded and tested conv2d:")
    print("scores:" + str(scores_loaded))

    predictions = model.predict(X_test)
    pp =[]
    for y, y_hat in zip(Y_test, predictions):
        #print(np.shape(y))
        hp_ppf = get_output_hp_ppf(y)
        hp_ppf_hat =get_output_hp_ppf(np.multiply(y_hat, output_bitmask()) )
        e_temp =[]
        for val, pred in zip (hp_ppf, hp_ppf_hat):
            e_temp.append(np.absolute(val - pred))
        pp.append(e_temp[-1])
        overall_errors.append(e_temp)
    rms_out2 = [x / len(Y_test) for x in rms_out]
    rms_error = [x for x in rms_out2]
    np_oe = np.array([w[:1] for w in overall_errors])
    np_pp_abs = np.array([w[-1] for w in overall_errors])
    np_pp2 = np.array([w[-1]/pp for w,pp in zip(overall_errors, pp)])
    filename  = "stats2.pickle"
    stats = pickle.load( open (filename, "rb") )
    if stats.first :
        stats.errors2 = [np.mean(scores_loaded)]
        stats.pp2 = [np_pp2]
        stats.scores_frozen = [scores_loaded]
    else:
        stats.errors2.append(np.mean(scores_loaded))
        stats.pp2.append(pp)
        stats.scores_frozen.append(scores_loaded)
    #stats.errors2.append(scores_frozen[1]) if stats.errors2  else stats.errors3 = [scores_frozen[1]]
    #stats.scores_loaded.append(scores_loaded)
    #stats.pp2.append(np.mean(pp)) if stats.pp2 else stats.pp2 = [np.mean[pp]]
    pickle.dump(stats, open( filename, "wb" ) )
    print (stats)
################################################################################
    model_load = Sequential()
    model_file = "cnn_model_200510.hdf5"
    #model.save(model_file)
    #model.save_weights("weights_"+model_file)
    model_load = load_model(model_file)

    model = Sequential()
    model.add(Conv2D(3, (3, 3), input_shape=(64, 63, 3), name="conv_one", kernel_initializer='random_uniform' ))
    model.layers[0].set_weights(model_load.layers[0].get_weights())
    model.add(Activation('tanh'))
    model.add(AveragePooling2D(pool_size=(2, 2)))
    #model.add(MaxPooling2D())

    model.add(Conv2D(6, (3, 3), name="conv_two", kernel_initializer='random_uniform'))
    model.layers[3].set_weights(model_load.layers[3].get_weights())
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    #model.add(MaxPooling2D())

    model.add(Conv2D(3, (3, 3), name="conv_three", kernel_initializer='random_uniform' ))
    model.layers[6].set_weights(model_load.layers[6].get_weights())
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    #model.add(MaxPooling2D())
    # the model so far outputs 3D feature maps (height, width, features)
    model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
#    model.add(Dense(64))
#    model.add(Activation('relu'))

    model.add(Dense(3000, kernel_initializer='normal', activation='tanh'))
    for n in range(mlp_layers):
        model.add(Dense(neurons_per_layer, kernel_initializer='normal', activation='tanh'))
    for l in range(11,14):
        model.layers[l].set_weights(model_load.layers[l].get_weights())
    model.add(Dense(num_outputs, kernel_initializer='normal'))
    model.add(Reshape((68, 68)))

    # and freeze!
    model.layers[0].trainable = False
    model.layers[1].trainable = False
    model.layers[2].trainable = False
    model.layers[3].trainable = False
    model.layers[4].trainable = False
    model.layers[5].trainable = False
    model.compile(loss='mae', optimizer='adam', metrics=['mae'])
    model.summary()
    model.fit(X_train, Y_train, epochs=100, batch_size=50, verbose=0)
    scores_frozen = model.evaluate(X_test, Y_test)
    print("Reloaded and frozen conv2d:")
    print("scores:" + str(scores_frozen))


    predictions = model.predict(X_test)
    pp =[]
    for y, y_hat in zip(Y_test, predictions):
        #print(np.shape(y))
        hp_ppf = get_output_hp_ppf(y)
        hp_ppf_hat =get_output_hp_ppf(np.multiply(y_hat, output_bitmask()) )
        e_temp =[]
        for val, pred in zip (hp_ppf, hp_ppf_hat):
            e_temp.append(np.absolute(val - pred))
        pp.append(e_temp[-1])
    rms_out2 = [x / len(Y_test) for x in rms_out]
    rms_error = [x for x in rms_out2]
    np_oe = np.array([w[:1] for w in overall_errors])
    np_pp_abs = np.array([w[-1] for w in overall_errors])
    np_pp3 = np.array([w[-1]/pp for w,pp in zip(overall_errors, pp)])
    filename = "stats2.pickle"
    stats = pickle.load( open (filename, "rb") )
    if stats.first :
        stats.errors3 = [np.mean(scores_frozen)]
        stats.np_pp3 = [np_pp3]
        stats.scores_frozen = [scores_frozen]
    else:
        stats.errors3.append(np.mean(scores_frozen))
        stats.pp3.append(pp)
        filename.scores_frozen.append(scores_frozen)
    filename.first = False
    pickle.dump(stats, open( filename, "wb" ) )

if __name__ == "__main__":
    main()
