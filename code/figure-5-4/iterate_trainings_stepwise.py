
import pandas as pd
import sys, os
import numpy as np
import time
import random as rn
import tensorflow as tf

from timeit import default_timer as timer

import matplotlib.pyplot as plt
import matplotlib.font_manager as font_manager
# Dark2 is found at: https://matplotlib.org/tutorials/colors/colormaps.html done badly here! TODO:fix
dark2 =["#1b9e77", "#d95f02", "#7570b3", "#e7298a", "#66a61e", "#e6ab02", "#a6761d", "#666666", "#c61e11"]
dark2 =["#1b9e77", "#d95f02", "#7570b3", "#e7298a", "#66a61e", "#e6ab02", "#a6761d", "#666666"]
fig_font = {'fontname':'Liberation Serif'}
font = font_manager.FontProperties(family='Liberation Serif')

mlp_layers = 9
neurons_per_layer = 80#125

cnn_layers = 3
cnn_neurons_per_layer = 250

mlp_epochs= 500 # 500
cnn_epochs= 100 # 100
seed  = 221179#sys.argv[2]

#splits = [0.15625, 0.3125, 0.625, 0.9375] # 500,1000,2000, 3000
#               100                     500                     750                     1000                   1250                    1500                     1750                    2000                    2250                    2500                    2750                  3000     3200
splits =       [0.03125,                0.15625,                0.234375,               0.3125,                 0.390625,               0.46875,                0.546875,               0.625,                  0.703125,               0.78125,                0.859375,             0.9375, 0.9996875]
splits_sobol = [0.03334444814938312771, 0.16672224074691563855, 0.25008336112037345782, 0.33344448149383127709, 0.41680560186728909637, 0.50016672224074691564, 0.58352784261420473491, 0.66688896298766255418, 0.75025008336112037346, 0.83361120373457819273, 0.916972324108036012, 0.99999]
# Max for PYTHONHASHSEED 4294967295
# Max for numpy 2**32
if seed == 'time':
    # shuffled time initialisation source:
    # https://github.com/schmouk/PyRandLib
    #t = int( time.time() * 1000.0 )
    #seed = str( ( ((t & 0x0f000000) >> 24) +
    #            ((t & 0x00ff0000) >>  8) +
    #            ((t & 0x0000ff00) <<  8) +
    #            ((t & 0x000000ff) << 24)   )  )
    seed = str(int( (time.time()*1000.0) % 2147483647))
#os.environ['PYTHONHASHSEED'] = '0'
np.random.seed(int(seed))
#rn.seed(np.random.randint(0, 2**32 - 1, dtype='l'))
rn.seed(np.random.randint(0, 2147483647, dtype='l'))
#
from keras import backend as K
#tf.set_random_seed(np.random.randint(0, 2147483647, dtype='l'))
#session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)
#sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)
#K.set_session(sess)

from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.models import save_model
#from keras.models import model_from_json
from keras.optimizers import Adam
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
import data_store as ds
import pickle

from keras.layers import Conv2D, AveragePooling2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.layers.core import Reshape


description = "\nA script to repeatedly train the nns\n\n"
example_usage = "usage : \n" + str(sys.argv[0])+ " iterations \n\n eg:\n python3 "+str(sys.argv[0])+" ./results_190430_serp/runs/ time\n\n"

def usage_error(string = ""):
    print ("\n\nUSAGE ERROR: \n"+string)
    print (str(sys.argv[0]) + description + example_usage)
    quit()


mlp_errors = []
#training the mlp:

def get_hot_pin_ppf(d):
    full = np.block([[d[0], d[1], d[2]],
                     [d[3], d[4], d[5]],
                     [d[6], d[7], d[8]] ])
    #print(np.shape(full))
    hot_pin = np.unravel_index(full.argmax(), full.shape)
    max = np.max(full)
    full[full == 0.0] = np.nan
    mean_no_zero = np.nanmean(full.ravel())
    ppf = max / mean_no_zero
    return (list(hot_pin) + [ppf])

def get_input_image(nine_vars, filename='input_img.png', save=False):
    w, h = 55, 55
    control_rods = [[[ 6,  3], [ 9,  3], [12,  3],
                    [ 4,  4], [14,  4],
                    [ 3,  6], [ 6,  6], [ 9,  6], [12,  6], [15,  6],
                    [ 3,  9], [ 6,  9], [12,  9], [15,  9],
                    [ 3, 12], [ 6, 12], [ 9, 12], [12, 12], [15, 12],
                    [ 4, 14], [14, 14], [ 6, 15],
                    [ 9, 15], [12, 15]]
                   ]
    control_rods.append([ [v[0]+18,v[1]+ 0] for v in control_rods[0] ])
    control_rods.append([ [v[0]+36,v[1]+ 0] for v in control_rods[0] ])
    control_rods.append([ [v[0]+ 0,v[1]+18] for v in control_rods[0] ])
    control_rods.append([ [v[0]+18,v[1]+18] for v in control_rods[0] ])
    control_rods.append([ [v[0]+36,v[1]+18] for v in control_rods[0] ])
    control_rods.append([ [v[0]+ 0,v[1]+36] for v in control_rods[0] ])
    control_rods.append([ [v[0]+18,v[1]+36] for v in control_rods[0] ])
    control_rods.append([ [v[0]+36,v[1]+36] for v in control_rods[0] ])

    data = np.zeros((h, w, 3), dtype=np.uint8)
    for x in range(w):
        for y in range(h):
            assembly_number = int(x/18) + (int(y/18) *3)
            if x%18 == 0 or y%18==0 or assembly_number > 9:  # water gap
                data[y, x] = [0, 0, 255]
            else:
                if [x,y] in control_rods[assembly_number] or 0== x or 0==y or w-1 == x or h-1 == y:
                    data[y, x] = [0, 0, 255]
                else:
                    data[y, x] = [nine_vars[assembly_number]*50, 0, 0]
    if save:
        img = Image.fromarray(data, 'RGB')
        new_p.save(filename, "PNG")
    return data

def get_output_hp_ppf(d):
    full = d
    full[full == 0.0] = np.nan
    max = np.nanmax(full)
    mean_no_zero = np.nanmean(full.ravel())
    ppf = max / mean_no_zero
    hot_pin = np.unravel_index(full.argmax(), full.shape)
    return (list(hot_pin) + [ppf])

def get_output_image(d, filename='pin_power.png', save=False):
    full = np.block([[d[0], d[1], d[2]],
                     [d[3], d[4], d[5]],
                     [d[6], d[7], d[8]] ])
    full[full == 0.0] = np.nan
    max = np.nanmax(full) # largest value
    min = np.nanmin(full)
    full[full == np.nan] = 0.0
    old_range = max - min
    scaled = ( ( (full -min) * 1)/ old_range)+0.2
    scaled[scaled == np.nan] = 0
    #img.convert('RGB')
    if save:
        img = Image.fromarray(scaled*255)
        print(full)
        img.show()
        new_p = img.convert("L")
        new_p.save(filename, "PNG")
    return full


def main():
    mlp_train_size_store  = []
    mlp_performance_store = []
    cnn_train_size_store  = []
    cnn_performance_store = []
    cnn_perform_ppf_store = []

    mlp_train_size_sobol  = []
    mlp_performance_sobol = []
    cnn_train_size_sobol  = []
    cnn_performance_sobol = []
    cnn_perform_ppf_sobol = []

    random_mlp_data = ds.data_store()
    random_cnn_data = ds.data_store()
    sobol_mlp_data  = ds.data_store()
    sobol_cnn_data  = ds.data_store()

    if 2 != len(sys.argv) or sys.argv[1] == "--help":
        usage_error()
    iterations = int(sys.argv[1])
    pickle_dir = "./random_training_set/"#sys.argv[1]
    pickle_dir_sobol = "./sobol_training_set/"#sys.argv[1]
    #pickle_dir = "../experiment_1_ppf_hotpin_outside/mlp_nn/results_190501_symcore/runs_4/"#sys.argv[1]
    #pickle_dir_sobol = "../experiment_1_ppf_hotpin_outside/training_data/runs_sobol1/"#sys.argv[1]
    pickle_dir =       "../../data/results_190501_symcore/runs_4/"
    pickle_dir_sobol = "../../data/runs_sobol1/"

    try:
        os.path.exists(pickle_dir)
    except:
        print("\n\ directory not found: " + pickle_dir + "\n")
        exit()
    #list files (just the pickles)
    file_list = [x for x in os.listdir(pickle_dir) if x.endswith(".pickle")]
    #run through the files in order.
    inputs = []
    outputs = []
    outputs_cnn = []
    for data_file in sorted(file_list, key=lambda a: a.split(".")[0]):
        #print(pickle_dir+"/"+data_file)
        try:
            pickle_file = open(pickle_dir+"/"+data_file, "rb")
            #print("opened file")
            var = pickle.load(pickle_file, encoding='latin1')
            pickle_file.close()
        except:
            print("error opening '"+data_file+"' pickle file")
            exit()
        else:
            inputs.append(var.enrichments)
            outputs_cnn.append(var.detector_data)
            outputs.append(get_hot_pin_ppf(var.detector_data))
    num_inputs = len(inputs[0])
    num_outputs= len(outputs[0])

    #scales...

    X =  np.multiply(np.array(inputs), 1.0/5 )
    Y =  np.multiply(np.array(outputs), np.array([ 1.0/51, 1.0/51, 0.5]))

    #X, Y = preprocess(inputs, outputs)

    print(X[0].shape)
    print(Y[0].shape)
    X_train_full, X_test, Y_train_full, Y_test = train_test_split(X, Y, test_size = 0.2)

    data_file ="random_mlp_data.pickle"
    if os.path.exists(data_file) == False:
        for split in splits:
            mlp_performance=[]
            mlp_train_size=[]
            for i in range(iterations):
                X_train, X_test_do_not_use, Y_train, Y_test_do_not_use = train_test_split(X_train_full, Y_train_full, test_size = (1-split) )
                print("Random, MLP: iteration %d of %d, training size %d" % (i, iterations, len(X_train) ) )
                mlp_model = Sequential()
                mlp_model.add(Dense(150, input_dim=num_inputs, kernel_initializer='normal', activation='relu'))
                for n in range(mlp_layers):
                	mlp_model.add(Dense(neurons_per_layer, kernel_initializer='normal', activation='relu'))
                mlp_model.add(Dense(num_outputs, kernel_initializer='normal'))
                #model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mae']) #
                mlp_model.compile(loss='mae', optimizer='adam', metrics=['mae']) #
                # train the model
                start_tr = timer()
                mlp_model.fit(X_train, Y_train, epochs=mlp_epochs, batch_size=50, verbose=0) # usually verbose 1
                end_tr = timer()
                # test
                scores = mlp_model.evaluate(X_test, Y_test)
                print("\nperformance on test set:" + str(scores[1]))
                print("scores:"+str(scores))
                start_t = timer()
                predictions = mlp_model.predict(X_test)
                end_t = timer()
                num_runs = len(X_train)
                mlp_train_size.append(num_runs)

                rms_out = [0] * num_outputs
                for y, y_hat in zip(Y_test, predictions):
                    for i, (val, val_hat) in enumerate(zip (y, y_hat)):
                        rms_out[i] += np.absolute((val-val_hat))
                rms_out = [x / len(Y_test) for x in rms_out]
                rms_error = [x for x in rms_out]
                #mlp_performance.append(scores[1])
                mlp_performance.append(rms_error[1])
            mlp_train_size_store.append(np.mean(mlp_train_size))
            mlp_performance_store.append(np.mean(mlp_performance))
        random_mlp_data.mlp_performance_store = mlp_performance_store
        random_mlp_data.mlp_train_size_store = mlp_train_size_store
        try:
            pickle.dump(random_mlp_data, open(data_file, "wb+" ) )
        except:
            print("error saving '"+data_file+"' pickle file")
            exit()
    else:
        with open(data_file,'rb') as pickle_file:
            random_mlp_data = pickle.load(pickle_file, encoding='latin1')
        mlp_performance_store = random_mlp_data.mlp_performance_store
        mlp_train_size_store = random_mlp_data.mlp_train_size_store


    try:
        os.path.exists(pickle_dir)
    except:
        print("\n\ directory not found: " + pickle_dir + "\n")
        exit()
    #list files (just the pickles)
    file_list = [x for x in os.listdir(pickle_dir) if x.endswith(".pickle")]
    #run through the files in order.
    #print (str(file_list))
    inputs = []
    outputs = []
    for data_file in sorted(file_list, key=lambda a: a.split(".")[0]):
        #print(pickle_dir+"/"+data_file)
        try:
            pickle_file = open(pickle_dir+"/"+data_file, "rb")
            #print("opened file")
            var = pickle.load(pickle_file, encoding='latin1')
            pickle_file.close()
        except:
            print("error opening '"+data_file+"' pickle file")
            exit()
        else:
            inputs.append(get_input_image(var.enrichments))
            #outputs.append( np.concatenate( var.detector_data, axis=0 ) )
            #outputs.append([get_output_image(var.detector_data)]+ [get_hot_pin_ppf(var.detector_data)])
            outputs.append(get_output_image(var.detector_data))
#            np.set_printoptions(threshold=np.inf)
#            print(outputs[0])
#            exit()
#    num_inputs = len(inputs[0])
    num_outputs= 51*51

    #scales...
    #in_scaler = MinMaxScaler()
    #print(in_scaler.fit(inputs))
    #X = in_scaler.transform(data)

    X =  np.multiply(np.array(inputs), 1.0/5 )

#    print(np.shape([x[0] for x in outputs]))
#    print(np.shape([x[1] for x in outputs]))
    pin_pows =  outputs
#    print("nan in pin_pows:" +str(np.isnan(np.sum(pin_pows))))
    max = np.nanmax(np.array(pin_pows).flatten()) # largest value
#    print("the max values is : " + str(max))
    min = np.nanmin(np.array(pin_pows).flatten())
#    print("the min values is : " + str(min))
    pin_pows = np.nan_to_num(pin_pows)
#    print("nan in pin_pows:" +str(np.isnan(np.sum(pin_pows))))
    #outputs[outputs == np.nan] = 0.0
    old_range = max-min
    print(max)
    print(min)
    print(old_range)
#
    #Y = [[np.multiply(np.array(x[0]),scale_factor)]+[x[1]] for x in outputs]
    old_range = max-min
    offset = 0.0
    scale_factor = ( ( (1 -min) * 1) / old_range)+0.1
    print(scale_factor)
    scaled_set = ( ( (pin_pows -min) * 1)/ old_range) + offset
    save_scale_factors = ["list of format: desc, input_range, max, min, offset", old_range, max, min, offset]
    Y = scaled_set
    #print(Y)
    print("nan in outputs:" +str(np.isnan(np.sum(outputs))))
    print("nan in Y:" +str(np.isnan(np.sum(Y))))

    #use the same training set throughtout.
    X_train_full, X_test2, Y_train_full, Y_test2 = train_test_split(X, Y, test_size = 0.2)
    data_file ="random_cnn_data.pickle"
    if os.path.exists(data_file) == False:
        for split in splits:
            cnn_performance=[]
            cnn_train_size=[]
            cnn_performance_pp=[]
            for i in range(iterations):
                X_train, X_test_do_not_use, Y_train, Y_test_do_not_use = train_test_split(X_train_full, Y_train_full, test_size = 1-split)
                print("Random CNN: iteration %d of %d, training size %d" % (i, iterations, len(X_train) ) )
                cnn_model = Sequential()
                cnn_model.add(Conv2D(3, (3, 3), input_shape=(55, 55, 3), name="conv_one", kernel_initializer='random_uniform' ))
                cnn_model.add(Activation('tanh'))
                cnn_model.add(AveragePooling2D(pool_size=(2, 2)))
                #cnn_model.add(MaxPooling2D())

                cnn_model.add(Conv2D(6, (3, 3), name="conv_two", kernel_initializer='random_uniform'))
                cnn_model.add(Activation('relu'))
                cnn_model.add(MaxPooling2D(pool_size=(2, 2)))
                #cnn_model.add(MaxPooling2D())

                cnn_model.add(Conv2D(3, (3, 3), name="conv_three", kernel_initializer='random_uniform' ))
                cnn_model.add(Activation('relu'))
                cnn_model.add(MaxPooling2D(pool_size=(2, 2)))
                #cnn_model.add(MaxPooling2D())

                # the cnn_model so far outputs 3D feature maps (height, width, features)
                cnn_model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
            #    cnn_model.add(Dense(64))
            #    cnn_model.add(Activation('relu'))

                cnn_model.add(Dense(3000, kernel_initializer='normal', activation='tanh'))
                for n in range(cnn_layers):
                    cnn_model.add(Dense(cnn_neurons_per_layer, kernel_initializer='normal', activation='tanh'))
                cnn_model.add(Dense(num_outputs, kernel_initializer='normal'))
                cnn_model.add(Reshape((51, 51)))
            #    cnn_model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mae']) #
            #    cnn_model.compile(loss='mean_squared_logarithmic_error', optimizer='adam', metrics=['mae']) #
                cnn_model.compile(loss='mae', optimizer='adam', metrics=['mae'])

                start_tr = timer()
                cnn_model.fit(X_train, Y_train, epochs=cnn_epochs, batch_size=50, verbose=0)
            #    cnn_model.fit(X_train, Y_train, epochs=10, batch_size=100, verbose=1)
                end_tr = timer()
                # test
                scores = cnn_model.evaluate(X_test2, Y_test2)
                print("\nperformance on test set:" + str(scores[1]))
                print("scores:"+str(scores))

                start_t = timer()
                predictions = cnn_model.predict(X_test2)
                end_t = timer()
                num_runs = len(X_test2)

                print("%s: %.2f%%" % (cnn_model.metrics_names[1], scores[1]*100))

                rms_out = [0] * len(Y_test2)
                for y, y_hat in zip(Y_test2, predictions):
                    for i, (val, val_hat) in enumerate(zip (y, y_hat)):
                        rms_out[i] += np.absolute((val-val_hat))
                rms_out2 = [x / len(Y_test2) for x in rms_out]
                rms_error = [x for x in rms_out2]
                cnn_performance.append(np.mean(rms_error))
                rms_out = [0] * num_outputs
                overall_errors = []
                pp =[]
                for y, y_hat in zip(Y_test2, predictions):
                    #print(np.shape(y))
                    hp_ppf = get_output_hp_ppf(y)
                    hp_ppf_hat =get_output_hp_ppf(y_hat)
                    e_temp =[]
                    for val, pred in zip (hp_ppf, hp_ppf_hat):
                        e_temp.append(np.absolute(val - pred))
                    pp.append(hp_ppf[-1])
                    for i, (val, val_hat) in enumerate(zip (y, y_hat)):
                        rms_out[i] += np.absolute((val-val_hat))
                    overall_errors.append(e_temp)
                rms_out2 = [x / len(Y_test2) for x in rms_out]
                rms_error = [x for x in rms_out2]
                np_oe = np.array([w[:1] for w in overall_errors])
                np_pp_abs = np.array([w[-1] for w in overall_errors])
                np_pp = np.array([w[-1]/pp for w,pp in zip(overall_errors, pp)])
                #print ("results: "+str(overall_errors) )
                print ("averages:  "+str(np_oe.mean()) )
                print ("averages:  "+str(np_pp.mean()) )
                #cnn_performance.append(np.mean([np_oe.mean(), np_pp.mean()]))
                #cnn_train_size.append(len(X_train))
                #cnn_performance.append(np_oe.mean())
                #cnn_performance_pp.append(np_pp.mean())
                #cnn_train_size.append(len(X_train))
                cnn_performance_pp.append(np_pp.mean())
                cnn_train_size.append(len(X_train))
            cnn_train_size_store.append(np.mean(cnn_train_size))
            cnn_perform_ppf_store.append(np.mean(cnn_performance_pp))
            cnn_performance_store.append(np.mean(cnn_performance))
        #save
        random_cnn_data.cnn_train_size_store = cnn_train_size_store
        random_cnn_data.cnn_perform_ppf_store = cnn_perform_ppf_store
        random_cnn_data.cnn_performance_store = cnn_performance_store
        try:
            pickle.dump(random_cnn_data, open(data_file, "wb+" ) )
        except:
            print("error saving '"+data_file+"' pickle file")
            exit()
    else:
        with open(data_file,'rb') as pickle_file:
            random_mlp_data = pickle.load(pickle_file, encoding='latin1')
        cnn_train_size_store = random_cnn_data.cnn_train_size_store
        cnn_perform_ppf_store = random_cnn_data.cnn_perform_ppf_store
        cnn_performance_store = random_cnn_data.cnn_performance_store
################################################    SOBOL
    try:
        os.path.exists(pickle_dir_sobol)
    except:
        print("\n\ directory not found: " + pickle_dir_sobol + "\n")
        exit()
    #list files (just the pickles)
    file_list = [x for x in os.listdir(pickle_dir_sobol) if x.endswith(".pickle")]
    #run through the files in order.
    inputs = []
    outputs = []
    outputs_cnn = []
    for data_file in sorted(file_list, key=lambda a: a.split(".")[0]):
        print(pickle_dir_sobol+"/"+data_file)
        try:
            pickle_file = open(pickle_dir+"/"+data_file, "rb")
            print("opened file")
            var = pickle.load(pickle_file, encoding='latin1')
            pickle_file.close()
        except:
            print("error opening '"+data_file+"' pickle file")
            exit()
        else:
            inputs.append(var.enrichments)
            outputs_cnn.append(var.detector_data)
            outputs.append(get_hot_pin_ppf(var.detector_data))
    num_inputs = len(inputs[0])
    num_outputs= len(outputs[0])

    #scales...

    X =  np.multiply(np.array(inputs), 1.0/5 )
    Y =  np.multiply(np.array(outputs), np.array([ 1.0/51, 1.0/51, 0.5]))

    #X, Y = preprocess(inputs, outputs)

    print(X[0].shape)
    print(Y[0].shape)

    #X_train_full, X_test, Y_train_full, Y_test = train_test_split(X, Y, test_size = 0.2)
    #X_train_full, X_test, Y_train_full, Y_test = train_test_split(X, Y, test_size = 0.2)
    # sobol sequences are ordered...  also keep the old test sets...
    X_train_full = X[:]
    #X_test = X[3200:]
    Y_train_full = Y[:]
    #Y_test = Y[3200:]
    data_file ="sobol_mlp_data.pickle"
    if os.path.exists(data_file) == False:
        for split in splits_sobol:
            #mlp_performance_sobol=[]
            #mlp_train_size_sobol=[]
            mlp_performance=[]
            mlp_train_size=[]
            print("started training loop")
            for i in range(iterations):
                #X_train, X_test_do_not_use, Y_train, Y_test_do_not_use = train_test_split(X_train_full, Y_train_full, test_size = 1-split)
                X_train = X_train_full[:int(len(X_train_full) * split)]
                Y_train = Y_train_full[:int(len(Y_train_full) * split)]
                print("Sobol MLP: iteration %d of %d, training size %d" % (i, iterations, len(X_train) ) )
                mlp_model = Sequential()
                mlp_model.add(Dense(150, input_dim=num_inputs, kernel_initializer='normal', activation='relu'))
                for n in range(mlp_layers):
                	mlp_model.add(Dense(neurons_per_layer, kernel_initializer='normal', activation='relu'))
                mlp_model.add(Dense(num_outputs, kernel_initializer='normal'))
                #model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mae']) #
                mlp_model.compile(loss='mae', optimizer='adam', metrics=['mae']) #
                # train the model
                start_tr = timer()
                print("built model ... training...")
                mlp_model.fit(X_train, Y_train, epochs=mlp_epochs, batch_size=50, verbose=-0) # usually verbose 1
                end_tr = timer()
                # test
                scores = mlp_model.evaluate(X_test, Y_test)
                print("\nperformance on test set:" + str(scores[1]))
                print("scores:"+str(scores))

                start_t = timer()
                predictions = mlp_model.predict(X_test)
                end_t = timer()
                num_runs = len(X_train)
                mlp_train_size.append(num_runs)

                rms_out = [0] * num_outputs
                for y, y_hat in zip(Y_test, predictions):
                    for i, (val, val_hat) in enumerate(zip (y, y_hat)):
                        rms_out[i] += np.absolute((val-val_hat))
                rms_out = [x / len(Y_test) for x in rms_out]
                rms_error = [x for x in rms_out]
                mlp_performance.append(rms_error[1])
            mlp_performance_sobol.append(np.mean(mlp_performance))
            mlp_train_size_sobol.append(np.mean(mlp_train_size))
        sobol_mlp_data.mlp_performance_sobol = mlp_performance_sobol
        sobol_mlp_data.mlp_train_size_sobol = mlp_train_size_sobol
        try:
            pickle.dump(sobol_mlp_data, open(data_file, "wb+" ) )
        except:
            print("error saving '"+data_file+"' pickle file")
            exit()
    else:
        with open(data_file,'rb') as pickle_file:
            sobol_mlp_data = pickle.load(pickle_file, encoding='latin1')
        mlp_performance_sobol = sobol_mlp_data.mlp_performance_sobol
        mlp_train_size_sobol = sobol_mlp_data.mlp_train_size_sobol


    #graph it.
    try:
        os.path.exists(pickle_dir_sobol)
    except:
        print("\n\ directory not found: " + pickle_dir_sobol + "\n")
        exit()
    #list files (just the pickles)
    file_list = [x for x in os.listdir(pickle_dir_sobol) if x.endswith(".pickle")]
    #run through the files in order.
    #print (str(file_list))
    inputs = []
    outputs = []
    for data_file in sorted(file_list, key=lambda a: a.split(".")[0]):
        #print(pickle_dir+"/"+data_file)
        try:
            pickle_file = open(pickle_dir+"/"+data_file, "rb")
            #print("opened file")
            var = pickle.load(pickle_file, encoding='latin1')
            pickle_file.close()
        except:
            print("error opening '"+data_file+"' pickle file")
            exit()
        else:
            inputs.append(get_input_image(var.enrichments))
            #outputs.append( np.concatenate( var.detector_data, axis=0 ) )
            #outputs.append([get_output_image(var.detector_data)]+ [get_hot_pin_ppf(var.detector_data)])
            outputs.append(get_output_image(var.detector_data))
#            np.set_printoptions(threshold=np.inf)
#            print(outputs[0])
#            exit()
#    num_inputs = len(inputs[0])
    num_outputs= 51*51

    #scales...
    #in_scaler = MinMaxScaler()
    #print(in_scaler.fit(inputs))
    #X = in_scaler.transform(data)

    X =  np.multiply(np.array(inputs), 1.0/5 )

#    print(np.shape([x[0] for x in outputs]))
#    print(np.shape([x[1] for x in outputs]))
    pin_pows =  outputs
#    print("nan in pin_pows:" +str(np.isnan(np.sum(pin_pows))))
    max = np.nanmax(np.array(pin_pows).flatten()) # largest value
#    print("the max values is : " + str(max))
    min = np.nanmin(np.array(pin_pows).flatten())
#    print("the min values is : " + str(min))
    pin_pows = np.nan_to_num(pin_pows)
#    print("nan in pin_pows:" +str(np.isnan(np.sum(pin_pows))))
    #outputs[outputs == np.nan] = 0.0
    old_range = max-min
    #print(max)
    #print(min)
    #print(old_range)
#
    #Y = [[np.multiply(np.array(x[0]),scale_factor)]+[x[1]] for x in outputs]
    old_range = max-min
    offset = 0.0
    scale_factor = ( ( (1 -min) * 1) / old_range)+0.1
    #print(scale_factor)
    scaled_set = ( ( (pin_pows -min) * 1)/ old_range) + offset
    save_scale_factors = ["list of format: desc, input_range, max, min, offset", old_range, max, min, offset]
    Y = scaled_set
    #print(Y)
    print("nan in outputs:" +str(np.isnan(np.sum(outputs))))
    print("nan in Y:" +str(np.isnan(np.sum(Y))))

    X_train_full = X[:3200]
#    X_test = X[3200:]
    Y_train_full = Y[:3200]
#    Y_test = Y[3200:]
    data_file ="sobol_cnn_data.pickle"
    if os.path.exists(data_file) == False:
        for split in splits_sobol:
            cnn_performance=[]
            cnn_train_size=[]
            cnn_performance_pp=[]
            for i in range(iterations):
                #X_train, X_test_do_not_use, Y_train, Y_test_do_not_use = train_test_split(X_train_full, Y_train_full, test_size = 1-split)
                X_train = X_train_full[:int(len(X_train_full) * split)]
                Y_train = Y_train_full[:int(len(Y_train_full) * split)]
                print("Sobol CNN: iteration %d of %d, training size %d" % (i, iterations, len(X_train) ) )
                cnn_model = Sequential()
                cnn_model.add(Conv2D(3, (3, 3), input_shape=(55, 55, 3), name="conv_one", kernel_initializer='random_uniform' ))
                cnn_model.add(Activation('tanh'))
                cnn_model.add(AveragePooling2D(pool_size=(2, 2)))
                #cnn_model.add(MaxPooling2D())

                cnn_model.add(Conv2D(6, (3, 3), name="conv_two", kernel_initializer='random_uniform'))
                cnn_model.add(Activation('relu'))
                cnn_model.add(MaxPooling2D(pool_size=(2, 2)))
                #cnn_model.add(MaxPooling2D())

                cnn_model.add(Conv2D(3, (3, 3), name="conv_three", kernel_initializer='random_uniform' ))
                cnn_model.add(Activation('relu'))
                cnn_model.add(MaxPooling2D(pool_size=(2, 2)))
                #cnn_model.add(MaxPooling2D())

                # the cnn_model so far outputs 3D feature maps (height, width, features)
                cnn_model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
            #    cnn_model.add(Dense(64))
            #    cnn_model.add(Activation('relu'))

                cnn_model.add(Dense(3000, kernel_initializer='normal', activation='tanh'))
                for n in range(cnn_layers):
                    cnn_model.add(Dense(cnn_neurons_per_layer, kernel_initializer='normal', activation='tanh'))
                cnn_model.add(Dense(num_outputs, kernel_initializer='normal'))
                cnn_model.add(Reshape((51, 51)))
            #    cnn_model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mae']) #
            #    cnn_model.compile(loss='mean_squared_logarithmic_error', optimizer='adam', metrics=['mae']) #
                cnn_model.compile(loss='mae', optimizer='adam', metrics=['mae'])

                start_tr = timer()
                cnn_model.fit(X_train, Y_train, epochs=cnn_epochs, batch_size=50, verbose=0)
            #    cnn_model.fit(X_train, Y_train, epochs=10, batch_size=100, verbose=1)
                end_tr = timer()
                # test
                scores = cnn_model.evaluate(X_test2, Y_test2)
                print("\nperformance on test set:" + str(scores[1]))
                print("scores:"+str(scores))

                start_t = timer()
                predictions = cnn_model.predict(X_test2)
                end_t = timer()
                num_runs = len(X_test2)

                print("%s: %.2f%%" % (cnn_model.metrics_names[1], scores[1]*100))

                rms_out = [0] * len(Y_test2)
                for y, y_hat in zip(Y_test2, predictions):
                    for i, (val, val_hat) in enumerate(zip (y, y_hat)):
                        rms_out[i] += np.absolute((val-val_hat))
                rms_out2 = [x / len(Y_test2) for x in rms_out]
                rms_error = [x for x in rms_out2]
                cnn_performance.append(np.mean(rms_error))
                rms_out = [0] * num_outputs
                overall_errors = []
                pp =[]
                per_pin_rel_error = []
                for y, y_hat in zip(Y_test2, predictions):
                    #print(np.shape(y))
                    hp_ppf = get_output_hp_ppf(y)
                    hp_ppf_hat =get_output_hp_ppf(y_hat)
                    e_temp =[]
                    e_temp_rel = []
                    for val, pred in zip (hp_ppf, hp_ppf_hat):
                        e_temp.append(np.absolute(val - pred))
                        e_temp_rel.append(np.absolute(val - pred)/val)
                    pp.append(hp_ppf[-1])
                    for i, (val, val_hat) in enumerate(zip (y, y_hat)):
                        rms_out[i] += np.absolute((val-val_hat))
                    overall_errors.append(e_temp)
                    per_pin_rel_error.append(e_temp_rel)
                rms_out2 = [x / len(Y_test2) for x in rms_out]
                rms_error = [x for x in rms_out2]
                np_oe = np.array([w[:1] for w in overall_errors])
                np_pp_abs = np.array([w[-1] for w in overall_errors])
                np_pp = np.array([w[-1]/pp for w,pp in zip(overall_errors, pp)])
                #print ("results: "+str(overall_errors) )
                print ("averages:  "+str(np_oe.mean()) )
                print ("averages:  "+str(np_pp.mean()) )
                #cnn_performance.append(np_oe.mean())
                #cnn_performance_pp.append(np_pp.mean())
                #cnn_train_size.append(len(X_train))
                #cnn_performance.append(np.mean(rms_error))
                cnn_performance_pp.append(np_pp.mean())
                cnn_train_size.append(len(X_train))
            cnn_train_size_sobol.append(np.mean(cnn_train_size))
            cnn_perform_ppf_sobol.append(np.mean(cnn_performance_pp))
            cnn_performance_sobol.append(np.mean(cnn_performance))
        #save
        sobol_cnn_data.cnn_train_size_store = cnn_train_size_sobol
        sobol_cnn_data.cnn_perform_ppf_store = cnn_perform_ppf_sobol
        sobol_cnn_data.cnn_performance_store = cnn_performance_sobol
        try:
            pickle.dump(sobol_cnn_data, open(data_file, "wb+" ) )
        except:
            print("error saving '"+data_file+"' pickle file")
            exit()
    else:
        with open(data_file,'rb') as pickle_file:
            sobol_mlp_data = pickle.load(pickle_file, encoding='latin1')
        cnn_train_size_sobol = sobol_cnn_data.cnn_train_size_sobol
        cnn_perform_ppf_sobol = sobol_cnn_data.cnn_perform_ppf_sobol
        cnn_performance_sobol = sobol_cnn_data.cnn_performance_sobol
    ################################# SOBOL

    # Plot
    fig, ax = plt.subplots()
    plt.plot(mlp_train_size_store, [y*100.0 for y in mlp_performance_store], '2-', color=dark2.pop(), alpha=0.9, label='MLP, random training set')#'o-',
    dark2.pop()
    plt.plot(cnn_train_size_store, [y*100.0 for y in cnn_perform_ppf_store], '4-', color=dark2.pop(), alpha=0.9, label='CNN, random training set, PPF error')#'o-',
    plt.plot(mlp_train_size_sobol, [y*100.0 for y in mlp_performance_sobol], '2-', color=dark2.pop(), alpha=0.9, label='MLP, Sobol training set, ')#'o-',
    plt.plot(cnn_train_size_sobol, [y*100.0 for y in cnn_perform_ppf_sobol], '4-', color=dark2.pop(), alpha=0.9, label='CNN, Sobol training set, PPF error')#'o-',
    #dark2.pop()
    #dark2.pop()

    ax.set_title('Size of training set vs MAE for PPF prediction', **fig_font)
    ax.set_ylabel('MAE / %', **fig_font)
    ax.set_xlabel('Training set size', **fig_font)

    ax.legend(loc=1, prop=font)
    fig.savefig('training_size_vs_error.svg')

    fig, ax = plt.subplots()
    plt.plot(cnn_train_size_sobol, [y*100.0 for y in cnn_performance_sobol], '3-', color=dark2.pop(), alpha=0.9, label='CNN, Sobol training set, pin power error')
    plt.plot(cnn_train_size_store, [y*100.0 for y in cnn_performance_store], '3-', color=dark2.pop(), alpha=0.9, label='CNN, random training set, pin power error')#'o-',
    ax.set_title('Size of training set vs MAE', **fig_font)
    ax.set_ylabel('MAE / %', **fig_font)
    ax.set_xlabel('Training set size', **fig_font)

    ax.legend(loc=1, prop=font)
    fig.savefig('training_cnn_vs_error.svg')

    #plt.show()
    print("these should be the same...")
#    print(mlp_train_size_store)#
#    print(cnn_train_size_store)
    print(mlp_train_size_sobol)
    print(cnn_train_size_sobol)
    print("these should be different")
#    print(mlp_performance_store)#
#    print(cnn_performance_store)
#    print(cnn_perform_ppf_store)
    print(mlp_performance_sobol)
    print(cnn_performance_sobol)
    print(cnn_perform_ppf_sobol)


if __name__ == "__main__":
    main()
