import copy
import numpy as np
import sys, os
import re
import data_store as ds
import pickle

import matplotlib.pyplot as plt

num_detector = 13

# from::https://stackoverflow.com/questions/42463172/how-to-perform-max-mean-pooling-on-a-2d-array-using-numpy
def pooling(mat,ksize,method='mean',pad=False):
    '''Non-overlapping pooling on 2D or 3D data.

    <mat>: ndarray, input array to pool.
    <ksize>: tuple of 2, kernel size in (ky, kx).
    <method>: str, 'max for max-pooling,
                   'mean' for mean-pooling.
    <pad>: bool, pad <mat> or not. If no pad, output has size
           n//f, n being <mat> size, f being kernel size.
           if pad, output has size ceil(n/f).

    Return <result>: pooled matrix.
    '''
    m, n = mat.shape[:2]
    ky,kx=ksize

    _ceil=lambda x,y: int(np.ceil(x/float(y)))

    if pad:
        ny=_ceil(m,ky)
        nx=_ceil(n,kx)
        size=(ny*ky, nx*kx)+mat.shape[2:]
        mat_pad=np.full(size,np.nan)
        mat_pad[:m,:n,...]=mat
    else:
        ny=m//ky
        nx=n//kx
        mat_pad=mat[:ny*ky, :nx*kx, ...]
    new_shape=(ny,ky,nx,kx)+mat.shape[2:]
    if method=='max':
        result=np.nanmax(mat_pad.reshape(new_shape),axis=(1,3))
    else:
        result=np.nanmean(mat_pad.reshape(new_shape),axis=(1,3))
    return result

def main():
    detector_data  = [copy.deepcopy(np.empty([17,17])) for x in range(num_detector)]
    detector_error = [copy.deepcopy(np.empty([17,17])) for x in range(num_detector)]
    full_smr       =  copy.deepcopy(np.empty([68,68]))
    full_smr[:]    = np.nan
    #special cases for the SMR since there is a half pin...
#    detector_data[0] = copy.deepcopy(np.empty([34,34]))
#    detector_data[1] = copy.deepcopy(np.empty([34,17]))
#    detector_data[2] = copy.deepcopy(np.empty([34,17]))
#    detector_data[3] = copy.deepcopy(np.empty([34,17]))#
#    detector_error[0] = copy.deepcopy(np.empty([34,34]))
#    detector_error[1] = copy.deepcopy(np.empty([34,17]))
#    detector_error[2] = copy.deepcopy(np.empty([34,17]))
#    detector_error[3] = copy.deepcopy(np.empty([34,17]))

    if len(sys.argv) == 2:
        detector_filename = sys.argv[1]+"_det0.m"
    else:
        detector_filename = "smr_det0.m"
    try:
        file = open(detector_filename, 'r')
    except in_file_error:
        usage_error("detector file error: (check path and that it exists!)")
        quit()
    else:
        filedata = file.readlines()
        for i in range(1, num_detector+1):
            temp_data = copy.deepcopy(filedata)
            parsing = False
            for line in temp_data:
                if parsing == False:
                    if "DETa{:03d} = [".format(int(i)) in line:
                        print("found " + "DETa{:03d} = [".format(int(i)))
                        parsing = True
                else:
                    if "];" in line:
                        parsing = False
                    else:
                        data  = [float(x) for x in re.findall(r"[+-]? *(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][+-]?\d+)?", line)]
                        print("y:"+str(int(data[-4])-1) + " x:"+str(int(data[-3])-1) +" i:"+str(i-1))
                        (detector_data[i-1])[int(data[-4])-1][int(data[-3])-1]  = data[-2]
                        (detector_error[i-1])[int(data[-4])-1][int(data[-3])-1] = data[-1]
    #for item in detector_data:
    #    print(item)
    #    input()

    #now rearrange the solutions fancy list rotations from : https://stackoverflow.com/questions/8421337/rotating-a-two-dimensional-array-in-python#8421412
    #central assembly reconstitute from a quarter
    temp1 = copy.deepcopy(detector_data[0])
    temp2 = list(zip(*reversed(temp1)))
    temp3 = temp1[::-1]
    temp4 = list(zip(*reversed(temp2)))
    temp5 = np.concatenate( (temp4, temp2), axis=0)  # top right and bottom left
    temp6 = np.concatenate( (temp3, temp1), axis=0)  # top left and bottom left
    temp7 = np.concatenate( (temp5, temp6), axis=1)
    #temp8 = skimage.measure.block_reduce(temp7, (2,2), np.mean)
    #temp8 = temp7.reshape(2, 17, 2, 17).mean(axis=(0, 2))
    temp8 = pooling(temp7, (2,2))
    #temp8 = temp7.reshape(17, 2, 17, 2).mean(axis=(1, 3))

    #plt.imshow(temp3, cmap='viridis')
    #plt.show()
    #exit()
    detector_data[0] =temp8

    #halves for 2 -> 4
    temp1 = copy.deepcopy(detector_data[1])
    temp2 = copy.deepcopy(detector_data[10])
    temp3 = list(zip(*reversed(temp2)))
    temp4 = list(zip(*reversed(temp3)))
    temp5 = list(zip(*reversed(temp4)))
    temp6 = np.concatenate( (temp5, temp1), axis=0)# np.transpose(temp2)), axis=0)
    #temp7 = skimage.measure.block_reduce(temp6, (1,2), np.mean)
    temp7 = pooling(temp6,(2,1))
    detector_data[1] =temp7

    temp1 = copy.deepcopy(detector_data[2])
    temp2 = copy.deepcopy(detector_data[11])
    temp3 = list(zip(*reversed(temp2)))
    temp4 = list(zip(*reversed(temp3)))
    temp5 = list(zip(*reversed(temp4)))
    temp6 = np.concatenate( (temp5, temp1), axis=0)# np.transpose(temp2)), axis=0)
    #temp7 = skimage.measure.block_reduce(temp6, (1,2), np.mean)
    temp7 = pooling(temp6,(2,1))
    detector_data[2] =temp7

    temp1 = copy.deepcopy(detector_data[3])
    temp2 = copy.deepcopy(detector_data[12])
    temp3 = list(zip(*reversed(temp2)))
    temp4 = list(zip(*reversed(temp3)))
    temp5 = list(zip(*reversed(temp4)))
    temp6 = np.concatenate( (temp5, temp1), axis=0)# np.transpose(temp2)), axis=0)
    #temp7 = skimage.measure.block_reduce(temp6, (1,2), np.mean)
    temp7 = pooling(temp6,(2,1))
    detector_data[3] =temp7

    filename="detector_data.png"
#    for i in range(num_detector):
#        plt.imshow(np.array(detector_data[i]), cmap='viridis')
#        plt.show()

    full_smr[ 0:17,  0:17] = detector_data[0]
    full_smr[ 0:17, 17:34] = detector_data[1]
    full_smr[ 0:17, 34:51] = detector_data[2]
    full_smr[ 0:17, 51:68] = detector_data[3]
    full_smr[17:34, 17:34] = detector_data[4]
    full_smr[17:34, 34:51] = detector_data[5]
    full_smr[17:34, 51:68] = detector_data[6]
    full_smr[34:51, 17:34] = detector_data[7]
    full_smr[34:51, 34:51] = detector_data[8]
    full_smr[51:68, 17:34] = detector_data[9]
#    cmap = plt.cm.viridis
#    cmap.set_bad((1, 1, 1, 0))
#    plt.imshow(full_smr, cmap='viridis')
#    plt.show()
#    exit()
#    plt.imsave(filename, full_smr, cmap=cmap)
#    exit()

    try:
        #print (os.path.splitext(detector_filename)[0]+".pickle")
        #pickle_file = open(os.path.splitext(detector_filename)[0]+".pickle", "rb")
        print (sys.argv[1]+".pickle")
        pickle_file = open(sys.argv[1]+".pickle", "rb")
        print("opened file")
        var = pickle.load(pickle_file)
        print(var.enrichments)
    except:
        print("error opening '' pickle file")
        exit()
    else:
        var.detector_data = detector_data
        pickle_file.close()
        try:
            f = open(sys.argv[1]+".pickle", "wb+")  # you should be creating state.pickle here...
        except:
            print ("Error opening output file")
            exit()
        else:
            f.write(pickle.dumps(var))
            f.close()

if __name__ == "__main__":
    main()
