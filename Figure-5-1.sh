#!/usr/bin/env bash
echo ""
echo "This script assumes that you have a copy of Serpent at:"
echo "/usr/software/Serpent/compile.2.1.30/sss2"
echo "please edit the script to point to your serpent installation"
echo ""
cd code/figure-5-1/
time /usr/software/Serpent/compile.2.1.30/sss2 -omp 5 -replay hpc_0000
mv ./lise_0000_mesh1.png ../../Figure_1_mesh1.png
echo "./Figure_1_mesh.png contains the image."
